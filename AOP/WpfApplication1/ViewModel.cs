﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApplication1
{
    class ViewModel : INotifyPropertyChanged
    {
        private string _text1;
        public string Text1
        {
            get { return _text1; }
            set
            {
                _text1 = value;
                OnPropertyChanged();
            }
        }

        public string Text2 { get; set; }

        public ICommand ChangeCommand { get; set; }

        public ViewModel()
        {
            Text1 = "Start 1";
            Text2 = "Start 2";

            ChangeCommand = new SimpleCommand<string>()
                                {
                                    ExecuteDelegate = s =>
                                                          {
                                                              Text1 = "Changed 1";
                                                              Text2 = "Changed 2";
                                                          }
                                };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
