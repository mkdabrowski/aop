﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly DbRepository _dbRepository;

        public HomeController()
        {
            _dbRepository = new DbRepository(ConfigurationManager.ConnectionStrings["Test"].ConnectionString);
        }

        //
        // GET: /Home/

        public ActionResult Index()
        {
            var balance = _dbRepository.Get(1);

            return View(balance);
        }

        public ActionResult Increase()
        {
            _dbRepository.Increase(1);

            return Redirect("Index");
        }
    }
}
