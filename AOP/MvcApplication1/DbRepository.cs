﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;

namespace MvcApplication1
{
    public class DbRepository
    {
        private readonly string _connectionString;

        public DbRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public int Get(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Query<int>(@"SELECT Balance FROM Account WHERE Id = @id", new { id });
                return result.FirstOrDefault();
            }
        }

        public void Increase(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute(@"UPDATE Account SET Balance = Balance + 100 WHERE Id = @id", new {id});
            }
        }
    }
}