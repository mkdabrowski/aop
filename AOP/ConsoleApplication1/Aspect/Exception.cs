﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PostSharp.Aspects;

namespace ConsoleApplication1
{
    [Serializable]
    public class ExceptionAttribute : OnExceptionAspect
    {
        [NonSerialized]
        private readonly Type _exceptionType;

        public ExceptionAttribute()
        {
        }

        public ExceptionAttribute(Type exceptionType)
        {
            _exceptionType = exceptionType;
        }

        public override Type GetExceptionType(System.Reflection.MethodBase targetMethod)
        {
            return _exceptionType;
        }

        public override void OnException(MethodExecutionArgs args)
        {
            Console.WriteLine("Ups, exception :"+ args.Exception);
            args.FlowBehavior = FlowBehavior.Continue;
        }
    }
}
