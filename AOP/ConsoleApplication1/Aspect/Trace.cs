﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using PostSharp.Aspects;

namespace ConsoleApplication1
{
    [Serializable]
    public sealed class TraceAttribute : OnMethodBoundaryAspect
    {
        // These fields are initialized at runtime. They do not need to be serialized.
        [NonSerialized]
        private string enteringMessage;
        [NonSerialized]
        private string exitingMessage;

        // Default constructor, invoked at build time. 
        public TraceAttribute()
        {
        }


        // Invoked only once at runtime from the static constructor of type declaring the target method. 
        public override void RuntimeInitialize(MethodBase method)
        {
            string methodName = method.DeclaringType.FullName + method.Name;
            this.enteringMessage = "Entering " + methodName;
            this.exitingMessage = "Exiting " + methodName;
        }

        // Invoked at runtime before that target method is invoked. 
        public override void OnEntry(MethodExecutionArgs args)
        {
            Console.WriteLine(this.enteringMessage);
        }

        // Invoked at runtime after the target method is invoked (in a finally block). 
        public override void OnExit(MethodExecutionArgs args)
        {
            Console.WriteLine(this.exitingMessage);
        }
    }
}
