﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using PostSharp.Aspects;

namespace ConsoleApplication1
{
    [Serializable]
    public sealed class TraceTimeAttribute : OnMethodBoundaryAspect
    {
        [NonSerialized]
        private DateTime _startTime;
        
        public override void OnEntry(MethodExecutionArgs args)
        {
            _startTime = DateTime.Now;
        }

        public override void OnExit(MethodExecutionArgs args)
        {
            Console.WriteLine("Time: " + (DateTime.Now - _startTime));
        }
    }
}
