﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PostSharp.Patterns.Diagnostics;
using PostSharp.Extensibility;

namespace ConsoleApplication1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var model = new Model();
            model.Divide(6, 2);
            model.Sum(2, 7);

            Console.ReadLine();
        }
    }

    public class Model
    {
        public int Divide(int a, int b)
        {
            var result = a / b;
            Console.WriteLine("{1}/{2}= {0}", result, a, b);
            return result;
        }

        public int Sum(int a, int b)
        {
            var result = a + b;
            Console.WriteLine("{1}+{2}= {0}", result, a, b);
            return result;
        }
    }
}